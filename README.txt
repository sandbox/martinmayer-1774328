
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Credits
 
INTRODUCTION
------------

Third Party integration of VideoIO videochat 
This module creates a tab in the user profile which contains a video chat pane. 


REQUIREMENTS
------------

Download the VideoIO library from http://sourceforge.net/projects/videoio/ and unpack it to sites/all/libraries/videoio
The Libraries module is required

You need a flash media (rtmp) server to run the software: Adobe, Red5, rtmplite or Wowza. This module was tested with Red5, which is available as Open Source at http://www.red5.org/

rtmplite is also Open Source as described at http://myprojectguide.org/p/flash-videoio/3.html

With Red5 you need an application which allows you to do live media stream. You may create it as described at http://fms.denniehoopingarner.com/newapp.html. The application at http://www.videowhisper.com/?p=RTMP+Applications described in Step 1 also works fine. It must be copied in a subdirectory of the Red5 installation folder as described.  

INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.

CONFIGURATION
-------------
Go to http://yourdrupaldomain/admin/config/development/videoio and insert the URL of the RTMP application resides on the media server (example: rtmp://your-server-or-ip/directory-of-application)

You can grant user rights separately for having a video chat tab in the profile and for accessing the video chat tabs of other users.

CREDITS
-------
Developer and maintainer: Martin Mayer (http://groups.drupal.org/user/30944)