function getFlashMovie(movieName) {
    var isIE = navigator.appName.indexOf("Microsoft") != -1;
    return (isIE) ? window[movieName] : document[movieName];  
}

function setVideoStreams(links, rechts) {
	var rtmp_url = Drupal.settings.videoio.rtmp_url;
	getFlashMovie('video1').setProperty('src', rtmp_url + '?publish=' + links);
    getFlashMovie('video2').setProperty('src', rtmp_url + '?play=' + rechts);
}

function resetVideoStreams() {
    var hurz = getFlashMovie('video2').setProperty('src', null);
	var hurz = getFlashMovie('video1').setProperty('src', null);
}

(function ($) {
Drupal.behaviors.videoio = {
  attach: function(context, settings) {
    $(document).ready(function(){
	  var ownerid = Drupal.settings.videoio.oid;
	  var visitorid = Drupal.settings.videoio.vid;
	  var ownername = Drupal.settings.videoio.oname;
	  var visitorname = Drupal.settings.videoio.vname;
      jQuery.get(Drupal.settings.basePath + 'videoio/arrive/' + ownerid + '/' + visitorid + '/'  + ownername + '/'   + visitorname, function(data, textStatus, jqXHR){ });
	  if(Drupal.settings.videoio.oid == Drupal.settings.videoio.vid){
	
	    jQuery.get( Drupal.settings.basePath + 'videoio/visitors' , function(data, textStatus, jqXHR){
	      if (jQuery.isEmptyObject(data)) {
			var no_visitors = Drupal.settings.videoio.no_visitors;
            $("#chatpartner").append(no_visitors + '<br/>');
	      } else {
	        jQuery.each(data, function(i, vname, oname, vid) {
	          var owner = this.oname;
	          var visitor = this.vname;
			  var visid = this.vid;
		      var selector = $('<button>' + visitor + '</button><br />');
		      selector.click(function() {
			    resetVideoStreams();
				setVideoStreams(owner, visitor);
				jQuery.get(Drupal.settings.basePath + 'videoio/choose/' + visid, function(data, textStatus, jqXHR){ });
              });
              $("#chatpartner").append(selector);
	        });
		    var resetb = $('<button>' + Drupal.settings.videoio.reset_button + '</button><br />');
		    resetb.click(function() {
			  resetVideoStreams();
              jQuery.get(Drupal.settings.basePath + 'videoio/choose/' + '0', function(data, textStatus, jqXHR){ });
            });
            $("#chatpartner").append(resetb);
	      }
	    });
	  } else {
	  	  jQuery.get( Drupal.settings.basePath + 'videoio/owner/' + ownerid , function(data, textStatus, jqXHR){
	      if (jQuery.isEmptyObject(data)) {
	        var oname = Drupal.settings.videoio.oname;
			var please_wait = Drupal.settings.videoio.please_wait;
            $("#chatpartner").append(please_wait + ' ' + oname + '<br/>');
//			resetVideoStreams();
	      } else {
	          var vname = Drupal.settings.videoio.vname;
	          var oname = Drupal.settings.videoio.oname;
              setVideoStreams(vname, oname);
	      }
	    });
	  }
    });
  }
};
})(jQuery);

(function ($) {
Drupal.behaviors.videoiu = {
  attach: function(context, settings) {
	$(window).unload(function(){
	  jQuery.post( Drupal.settings.basePath + 'videoio/unload');
	  resetVideoStreams();
	});
  }
};
})(jQuery);

(function ($) {
Drupal.behaviors.videoit = {
  attach: function(context, settings) {

     var meinInterval = setInterval(function() {
	  if(Drupal.settings.videoio.oid == Drupal.settings.videoio.vid){
	    jQuery.get( Drupal.settings.basePath + 'videoio/visitors' , function(data, textStatus, jqXHR){
//		 console.log(data);
          $("#chatpartner").empty();
	      if (jQuery.isEmptyObject(data)) {
			var no_visitors = Drupal.settings.videoio.no_visitors;
            $("#chatpartner").append(no_visitors + '<br/>');
	      } else {
	        jQuery.each(data, function(i, vname, oname, vid) {
	          var owner = this.oname;
	          var visitor = this.vname;
			  var visid = this.vid;
		      var selector = $('<button>' + visitor + '</button><br />');
		      selector.click(function() {
			    resetVideoStreams();
				setVideoStreams(owner, visitor);
				jQuery.get(Drupal.settings.basePath + 'videoio/choose/' + visid, function(data, textStatus, jqXHR){ });
              });
              $("#chatpartner").append(selector);
	        });
		    var resetb = $('<button>' + Drupal.settings.videoio.reset_button + '</button><br />');
		    resetb.click(function() {
			  resetVideoStreams();
              jQuery.get(Drupal.settings.basePath + 'videoio/choose/' + '0', function(data, textStatus, jqXHR){ });
            });
            $("#chatpartner").append(resetb);
	      }
	    });
	  } else {
	      var ownerid = Drupal.settings.videoio.oid;
	  	  jQuery.get( Drupal.settings.basePath + 'videoio/owner/' + ownerid , function(data, textStatus, jqXHR){
          $("#chatpartner").empty();
	      if (jQuery.isEmptyObject(data)) {
	        var oname = Drupal.settings.videoio.oname;
			var please_wait = Drupal.settings.videoio.please_wait;
            $("#chatpartner").append(please_wait + ' ' + oname + '<br/>');
			resetVideoStreams();
	      } else {
	          var vname = Drupal.settings.videoio.vname;
	          var oname = Drupal.settings.videoio.oname;
              setVideoStreams(vname, oname);
	      }
	    });
	  }
	}, 5000);
  }
};
})(jQuery);
