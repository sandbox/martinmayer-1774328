<?php
/**
* @file
* videoio module settings UI.
*/
/**
 * Form builder; Configure the videoio settings
 *
 * @see system_settings_form()
 */
function videoio_admin_settings() {
  $form = array();
  // videoio configuration settings.
  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('connection parameter'),
  );
  $form['display']['videoio_rtmp_url'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#maxlength' => 80,
    '#title' => t('URL of the rtmp application on the media server'),
    '#default_value' => variable_get('videoio_rtmp_url', ''),
/*    '#description' => t(' '); */
  );
  return system_settings_form($form);
}